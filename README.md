# Blockchain e-voting system front-end
This project is for the creation of an electronic voting dApp focused on general elections in Electoral Process Peru.

## Back - end
This is a kind of back-end because we have implemented a smart contract. In order to be able to use this smart
contract as back-end you have to deploy.

## Installation and Requirements
```sh
npm version 6.14.5
Node v12.18.1
Web3.js v1.2.1
Solidity - 0.5.8
Truffle v5.1.46
```

1. Install `Truffle` globally.
    ```sh
    npm install -g truffle lerna ganachi-cli
    ```

## Smart contract Deployment in the local blockchain
In the main directory:
- truffle compile
- truffle migrate --reset

And then you have to go the client side in order to run the front-end 

## Run the test chain

1. Run the test chain via

```
npm run chain
```

The test chain will always start with the same addeess/accounts. Check `Test chain detail` section for more detail.


### Front - end
Front-end [Create React App].
To run the front-end side you have to go to client directory:

## Installation and Requirements

```sh
npm version 6.14.5
npm install 
```

### `npm start`
Runs the "Blockchain e-voting system" in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!



