import {typeHandleChangeSelectionCandidate as handleChangeSelectionCandidate} from '../actions/electorSelectionsActions';
import {typeHandleGeneralInformationSelection as handleGeneralInformationSelection} from '../actions/electorSelectionsActions';

const defaultState = {
    index: -1,
    DNI: "",
    publicKey: "",
    castVote: false,
    listVotation:[]   //listVotation: [{politicalPartie: 1,candidate: "candidate1"}, ...]

};


function reducer(state = defaultState, {type,payload}) {
    switch (type) {
        case handleChangeSelectionCandidate : {
            console.log(state,payload);
            let newState = {...state};
            newState.listVotation[payload.posVotingList] = {selected: payload.selected, indexInList: payload.indexInList};

            console.log("change pos: [", payload.posVotingList,"] ",newState);
            return newState;
        }
        case handleGeneralInformationSelection: {
            console.log(state,payload);
            let newState = {...state};
            
            var listVotation = new Array(payload.lengthVotingList);
            for (let pos=0;pos <listVotation.length; pos++){
                listVotation[pos] = {selected: '', indexInList: -2};;
            }

            if (newState.listVotation.length === 0){
                newState.listVotation = listVotation;
            }
            newState.publicKey = payload.publicKey;
            newState.DNI = payload.DNI;
        
            console.log("state v.ini: ",newState);
            return newState;
        }
        default:
            return state;
    }
}

export default reducer;