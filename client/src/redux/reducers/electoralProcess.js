import { type as handleChange } from "../actions/electoralProcessActions";
import { typeHandleSaveElectoralProcess as handleSaveElectoralProcess } from "../actions/electoralProcessActions";
import { typeHandleCancelElectoralProcess as handleCancelElectoralProcess } from "../actions/electoralProcessActions";
import { typeValidateDate as validateDate } from "../actions/electoralProcessActions";
import dateTime from "../../components/ManageDates";

const defaultState = {
  title: "",
  description: "",
  dateIni: dateTime(),
  dateEnd: dateTime(),
  types: [
    { value: "unico", label: "Único candidato" },
    { value: "multiple", label: "Mútiples candidatos" },
  ],
  created: false,
};

function reducer(state = defaultState, { type, payload }) {
  switch (type) {
    case handleSaveElectoralProcess: {
      console.log(payload);
      //Llamar al servicio para que actualize la blockchain
      return {
        ...state,
        title: payload.title,
        description: payload.description,
        dateIni: payload.dateIni,
        dateEnd: payload.dateEnd,
        types: payload.types,
        created: true,
      };
    }
    case handleCancelElectoralProcess: {
      //Llamar al servicio para que resete los valores a los que se tenia previamente.
      return state;
    }

    case handleChange: {
      console.log(payload);
      return {
        ...state,
        [payload.name]: payload.value,
      };
    }
    case validateDate: {
      console.log(state);
      if (state.dateEnd < state.dateIni) {
        state.dateEnd = state.dateIni;
      }
      return state;
    }
    default:
      return state;
  }
}

export default reducer;
