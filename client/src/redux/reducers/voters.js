import {typeHandleUpload as handleUpload} from '../actions/votersActions';
import {typeProccesData as proccesData} from '../actions/votersActions';
import {typeHandleSaveDataVoters as handleSaveDataVoters} from '../actions/votersActions';
import { parse } from "papaparse";
import { data } from 'jquery';


const defaultState = {
    headers: [],
    data : [],
    codQr : [],
    file: null,
};



// const processData = async (stateBefore) => {
//     let state ={...stateBefore};
//     console.log(state, "procesando la data");

//     const text = await state.file.text()
//     const result = await parse(text, {header: true});
    
//     console.log("result",result);

//     let body = [];
//     await result.data.map((row) => {
//         body.push(row);
//     })
//     state.data = body;
//     state.headers = result.meta.fields;

//     console.log("state",state);
//     return state;
// }



function reducer(state = defaultState, {type,payload}) {
    switch (type) {
        case handleUpload: {
            console.log(type, payload);
            
            return {
                ...state,
                file: payload.file,
            }; 
        }
        case proccesData: {
            return state;
        }
        case handleSaveDataVoters: {
            return state;
        }
        //Llamar al servicio para que actualize la blockchain con el boton enviar correo 
        default:
            return state;
    }
}

export default reducer;