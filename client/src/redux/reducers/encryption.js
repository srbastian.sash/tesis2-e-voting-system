import { typeHandleChange_Qvalue as handleChange_Qvalue } from "../actions/encryptionActions";
import { typeHandleChange_Gvalue as handleChange_Gvalue } from "../actions/encryptionActions";
import { typeHandleAddAuditor as handleAddAuditor } from "../actions/encryptionActions";
import { typeHandleEditAuditor as handleEditAuditor } from "../actions/encryptionActions";
import { typeHandleDeleteAuditor as handleDeleteAuditor } from "../actions/encryptionActions";
import { typeHandleSetAuditors as handleSetAuditors } from "../actions/encryptionActions";
import { typeHandleChangeAuditorPrivateKey as handleChangeAuditorPrivateKey } from "../actions/encryptionActions";
import { typeHandleChangeAuditorPublicKey as handleChangeAuditorPublicKey } from "../actions/encryptionActions";

const defaultState = {
  q_upper_limit: "",
  g_generator: "",
  auditors: [],
  auditor_private_key: "",
  auditor_public_key: "",
};

function reducer(state = defaultState, { type, payload }) {
  switch (type) {
    case handleChangeAuditorPrivateKey: {
      let newState = { ...state };
      console.log(payload);
      newState.auditor_private_key = payload;
      return newState;
    }

    case handleChangeAuditorPublicKey: {
      let newState = { ...state };
      console.log(payload);
      newState.auditor_public_key = payload;
      return newState;
    }

    case handleSetAuditors: {
      let newState = { ...state };
      console.log(payload);
      let _auditors = [];
      payload.publicKeys.map((item, index) => {
        _auditors.push({
          publicKey: item,
          dni: payload.dnis[index],
          email: payload.emails[index],
        });
      });
      newState.auditors = _auditors;
      return newState;
    }

    case handleAddAuditor: {
      let newState = { ...state };

      newState.auditors.push({
        publicKey: payload.publicKey,
        dni: payload.dni,
        email: payload.email,
      });

      console.log(payload);
      return newState;
    }
    case handleEditAuditor: {
      console.log(payload);
      return state;
    }

    case handleDeleteAuditor: {
      console.log(payload);
      let newState = { ...state };
      let aux = newState.auditors;
      aux.splice(payload, 1);
      newState.auditors = aux;
      return newState;
    }

    case handleChange_Qvalue: {
      console.log(payload);
      let newState = { ...state };
      newState.q_upper_limit = payload;

      return newState;
    }

    case handleChange_Gvalue: {
      console.log("payload", payload);
      let newState = { ...state };
      newState.g_generator = payload;
      return newState;
    }

    default:
      return state;
  }
}

export default reducer;
