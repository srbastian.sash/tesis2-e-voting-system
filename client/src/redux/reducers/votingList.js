
import {typeHandleChangeVotingList as handleChangeVotingList} from '../actions/votingListActions';
import {typeHandleAddVotingList as handleAddVotingList} from '../actions/votingListActions';
import {typeHandleDeleteVotingList as handleDeleteVotingList} from '../actions/votingListActions';
import {typeHandleDeleteIteminVotingList as handleDeleteIteminVotingList}  from '../actions/votingListActions';
import {typeHandleAddIteminVotingList as handleAddIteminVotingList}  from '../actions/votingListActions';
import {typeHandleChangeSelection as handleChangeSelection}  from '../actions/votingListActions';
import {typeHandleChangeCheckPoliticalPartie as handleChangeCheckPoliticalPartie}  from '../actions/votingListActions';
import {typeHandleChangeCandidates as handleChangeCandidates}  from '../actions/votingListActions';
import {typeHandleUpdateVotingList as handleUpdateVotingList}  from '../actions/votingListActions';
import {typeHandleSaveListVoting as handleSaveListVoting}  from '../actions/votingListActions';
import {typeHandleCancelListVoting as handleCancelListVoting}  from '../actions/votingListActions';

const updateName = "updateName";
const deletePoliticalPartie = "deletePoliticalPartie";
const addPoliticalPartie = "addPoliticalPartie"; 
const changeCandidates = "changeCandidates";

const defaultState = [];

function reducer(state=defaultState, {type,payload}) {
    
    switch (type) {
        case handleSaveListVoting: {
            console.log(payload)
            //Llamar al servicio para que actualize la blockchain
            return payload;
        }
        case handleCancelListVoting: {
            //Llamar al servicio para que resetee todos los valores
            return state;
        }

        case handleUpdateVotingList: {
            let newState = [...state];
            if (state.length !== 0) {
                if (payload.name.localeCompare(updateName)=== 0) {
                    console.log("state", state," payload", payload);
                    newState.map((votingList) => {
                        votingList.politicalParties[payload.item.index].name = payload.item.value;
                    })

                }else  if(payload.name.localeCompare(deletePoliticalPartie)=== 0) {
                    console.log("state", state," payload", payload);
                    newState.map((votingList)=> {
                        votingList.politicalParties.splice(payload.item.itemSelected,1);
                    })
    
                }else if (payload.name.localeCompare(addPoliticalPartie)=== 0) {
                    console.log("state", state," payload", payload);
                    newState.map((votingList)=> {
                        const partie = {
                            isChecked:false,
                            name: payload.item.name,
                            candidates: payload.item.candidates,
                            selectedCandidates: []
                        }
                        votingList.politicalParties.push (partie);
                    })
    
                }else if(payload.name.localeCompare(changeCandidates)=== 0) {
                    console.log("state", state," payload",  payload);
                    newState.map((votingList)=> {
                        votingList.politicalParties.map((partie,index) => {
                            partie.candidates = Array.from(payload.item[index].candidates);
                            partie.selectedCandidates.map((selected, index) => {
                                let existCandidateInArray =false;
                                const arrayCandidates =partie.candidates;
                                for (let pos= 0 ; pos < arrayCandidates.length; pos++){
                                    if (selected.value.localeCompare(arrayCandidates[pos].value) === 0){
                                        existCandidateInArray= true;
                                        break;
                                    }
                                }
                                if (!existCandidateInArray){  //If the candidate doesnt exist in the array, => deleted candidate
                                    partie.selectedCandidates.splice(index, 1);
                                }
                                
                            })
                        })
                    })
    
                }
            }
            console.log("state", newState)
            return newState;
        }

        case handleChangeVotingList: {
            console.log(state,payload);
            let newState = [...state];
            newState[payload.index][payload.name] = payload.value;
            console.log(newState)
            return newState;
        }
        case handleChangeCandidates: {
            console.log("state",state," payload",payload);
            let newState = [...state];
            try {
                if (payload.value !== null) {
                    if (newState[payload.index]["type"]["value"].localeCompare("unico") === 0) {
                        if (payload.value.length > 0) {
                            let temp = [];
                            temp.push(payload.value[payload.value.length-1]);
                            console.log(temp);
                            newState[payload.index]["politicalParties"][payload.indexPoliticalPartie][payload.name] = temp;
                        }
                        else {
                            newState[payload.index]["politicalParties"][payload.indexPoliticalPartie][payload.name] = [];
                        }
                    } 
                    else {
                        newState[payload.index]["politicalParties"][payload.indexPoliticalPartie][payload.name] = payload.value;
                    }
                }
                else {
                    newState[payload.index]["politicalParties"][payload.indexPoliticalPartie][payload.name] = [];
                }
                console.log(newState);
                return newState;
            }catch(err) {
                console.log(err)
            }
            return state;
            
        }
        case handleAddVotingList : {
            let temp = []
            payload.map((item)=>{
                const partie = {
                    isChecked:false,
                    name: item.name,
                    candidates: item.candidates,
                    selectedCandidates: []
                }
                temp.push(partie)
            })
            
            console.log(payload)
            const item = {
                title: "",
                description: "",
                type: "",
                display: false,
                politicalParties: temp,
            };

            return [
                ...state,
                item
            ];
        }
        case handleChangeSelection : {
            console.log("handleChangeSelection",payload)
            let newState = [...state];
            newState[payload.index].display = true;
            newState[payload.index][payload.name] = payload.value;
            console.log(newState[payload.index]);

            if (newState[payload.index][payload.name].value.localeCompare("unico") === 0 ) {
                newState[payload.index].politicalParties.map((item) => {
                    if (item.selectedCandidates.length !== 0){
                        let temp = [];
                        item.selectedCandidates = temp.push(item.selectedCandidates[item.selectedCandidates.length -1]);
                    }

                })
            }
            return newState;
        }

        case handleChangeCheckPoliticalPartie : {
            console.log(payload)
            let newState = [...state];
            
            let isChecked = newState[payload.index]["politicalParties"][payload.position]["isChecked"]; 
            newState[payload.index]["politicalParties"][payload.position]["isChecked"] = !isChecked ;
                        
            return newState;
        }

        case handleDeleteVotingList : {
            let newState = [...state];
            newState.splice(payload.index,1);
            return newState;
        }
        case handleDeleteIteminVotingList : {
            return []
        }
        case handleAddIteminVotingList : {
            return []
        }
        default:
            return state;
    }
}

export default reducer;