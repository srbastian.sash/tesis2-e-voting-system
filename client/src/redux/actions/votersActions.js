export const typeHandleUpload = 'handleUpload';
export const typeProccesData = 'proccesData';
export const typeHandleSaveDataVoters = 'handleSaveDataVoters';


const handleUpload = (file) => {
    return {
        type: typeHandleUpload,
        payload: {file: file}, 
    };
}


const proccesData = () => {
    return {
        type: typeProccesData,
    };
}

const handleSaveDataVoters = (data) => {
    return {
        type: typeHandleSaveDataVoters,
        payload: data
    }
}

export {proccesData, handleUpload};