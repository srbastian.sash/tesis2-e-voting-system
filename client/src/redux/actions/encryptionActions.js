export const typeHandleChange_Qvalue = "handleChange_Qvalue";
export const typeHandleChange_Gvalue = "handleChange_Gvalue";
export const typeHandleAddAuditor = "handleAddAuditor";
export const typeHandleEditAuditor = "handleEditAuditor";
export const typeHandleDeleteAuditor = "handleDeleteAuditor";
export const typeHandleSetAuditors = "handleSetAuditors";
export const typeHandleChangeAuditorPrivateKey =
  "handleChangeAuditorPrivateKey";
export const typeHandleChangeAuditorPublicKey = "handleChangeAuditorPublicKey";

const handleSetAuditors = (dnis, publicKeys, emails) => {
  return {
    type: typeHandleSetAuditors,
    payload: { dnis: dnis, publicKeys: publicKeys, emails: emails },
  };
};

const handleAddAuditor = (dni, publicKey, email) => {
  return {
    type: typeHandleAddAuditor,
    payload: { dni: dni, publicKey: publicKey, email: email },
  };
};
const handleEditAuditor = (value, position) => {
  return {
    type: typeHandleEditAuditor,
    payload: { value: value, position: position },
  };
};

const handleDeleteAuditor = (value) => {
  return {
    type: typeHandleDeleteAuditor,
    payload: value,
  };
};

const handleChange_Qvalue = (value) => {
  return {
    type: typeHandleChange_Qvalue,
    payload: value,
  };
};

const handleChange_Gvalue = (value) => {
  return {
    type: typeHandleChange_Gvalue,
    payload: value,
  };
};
const handleChangeAuditorPrivateKey = (value) => {
  return {
    type: typeHandleChangeAuditorPrivateKey,
    payload: value,
  };
};
const handleChangeAuditorPublicKey = (value) => {
  return {
    type: typeHandleChangeAuditorPublicKey,
    payload: value,
  };
};

export {
  handleChangeAuditorPublicKey,
  handleChangeAuditorPrivateKey,
  handleSetAuditors,
  handleChange_Qvalue,
  handleChange_Gvalue,
  handleDeleteAuditor,
  handleEditAuditor,
  handleAddAuditor,
};
