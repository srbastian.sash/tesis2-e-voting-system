export const type = 'handleChange';
export const typeHandleSaveElectoralProcess = 'handleSaveElectoralProcess';
export const typeHandleCancelElectoralProcess = 'handleCancelElectoralProcess';
export const typeValidateDate = 'validateDate';

const handleChange = (value, name) => {
    return {
        type: type,
        payload: {name: name, value:value}, 
    };
}

const handleSaveElectoralProcess = (value) => {
    return {
        type: typeHandleSaveElectoralProcess,
        payload :value
    };
}
const handleCancelElectoralProcess = () => {
    return {
        type: typeHandleCancelElectoralProcess,
    };
} 
const validateDate = () => {
    return {
        type: typeValidateDate,
    };
} 

export  {validateDate, handleChange, handleSaveElectoralProcess, handleCancelElectoralProcess };