import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import LoginAdmin from "./components/admin/Login";
import LoginElector from "./components/elector/Login";
import HomeAdmin from "./components/admin/principal/Home";
import HomeElector from "./components/elector/principal/Home";
import store from "./redux/store";

const Root = (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/login-elector" component={LoginElector} />
        <Route exact path="/login-admin" component={LoginAdmin} />
        <Route exact path="/admin" component={HomeAdmin} />
        <Route exact path="/elector" component={HomeElector} />
        <Redirect from="/" to="login-elector" />
      </Switch>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(Root, document.getElementById("root"));
