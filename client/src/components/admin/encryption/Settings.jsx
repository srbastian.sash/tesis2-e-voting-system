import React, { Component, useState, useEffect } from "react";
import { connect } from "react-redux";
import Spinner from "react-bootstrap/Spinner";
import admin_style from "../admin_style.css";
import { toast } from 'toast-notification-alert';
import {
  handleChange_Gvalue,
  handleChange_Qvalue,
  handleEditAuditor,
  handleAddAuditor,
  handleDeleteAuditor,
  handleSetAuditors,
  handleChangeAuditorPrivateKey,
  handleChangeAuditorPublicKey,
} from "../../../redux/actions/encryptionActions";
import axios from "axios";

function ModalAddAuditor(props) {
  const [dni, setDni] = useState("");
  const [publicKey, setPublicKey] = useState("");
  const [email, setEmail] = useState("");
  console.log(props);
  return (
    <div
      id="modal-addAuditor"
      className="modal fade"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-success">
        <div className="modal-content" style={{ paddingBottom: "0px" }}>
          <div className="modal-header flex-column">
            <h3 className="modal-title w-100">Agregar nuevo auditor</h3>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-hidden="true"
            >
              ×
            </button>
          </div>
          <div
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            className="modal-body col-sm-12"
          >
            <form>
            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="100"
                  type="text"
                  title="clave pública"
                  name="publicKey"
                  required
                  className="form-control"
                  placeholder="Clave pública"
                  value={publicKey}
                  onChange={(e) => {
                    setPublicKey(e.target.value);
                  }}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="50"
                  type="text"
                  required
                  title="Correo electrónico"
                  name="email"
                  className="form-control"
                  placeholder="Correo electrónico"
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="8"
                  type="text"
                  title="dni"
                  name="dni"
                  className="form-control"
                  required
                  placeholder="DNI"
                  value={dni}
                  onChange={(e) => {
                    setDni(e.target.value);
                  }}
                />
              </div>
            </div>
            </form>
          </div>
          <div className="modal-footer justify-content-center">
            <button
              type="button"
              className="btn btn-secondary"
              style={{ marginRight: "30px" }}
              data-dismiss="modal"
              onClick={() => {
                setDni("");
                setPublicKey("");
              }}
            >
              Cancelar
            </button>
            <button
              type="button"
              className="btn btn-info"
              data-dismiss="modal"
              style={{ marginLeft: "30px" }}
              onClick={() => {
                console.log(dni, publicKey);
                const _dni = dni;
                const _publicKey = publicKey;
                const _email = email;

                setDni("");
                setPublicKey("");
                setEmail("");
                props.handleAddAuditor(_dni, _publicKey, _email);
              }}
            >
              Agregar
            </button>
          </div>
          
        </div>
      </div>
    </div>
  );
}

function ModalEditAuditor(props) {
  console.log(props);
  const [dni, setDni] = useState(props.auditorSelected.dni);
  const [publicKey, setPublicKey] = useState(props.auditorSelected.publicKey);
  const [email, setEmail] = useState(props.auditorSelected.email);

  useEffect(() => {
    setDni(props.auditorSelected.dni);
    setPublicKey(props.auditorSelected.publicKey);
  });

  return (
    <div
      id="modal-editAuditor"
      className="modal fade"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-success">
        <div className="modal-content" style={{ paddingBottom: "0px" }}>
          <div className="modal-header flex-column">
            <h3 className="modal-title w-100">Editar auditor</h3>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-hidden="true"
            >
              ×
            </button>
          </div>
          <div
            style={{ paddingTop: "5px", paddingBottom: "5px" }}
            className="modal-body col-sm-12"
          >
            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="50"
                  type="text"
                  title="clave pública"
                  name="publicKey"
                  className="form-control"
                  placeholder="Clave pública"
                  value={publicKey}
                  onChange={(e) => setPublicKey(e.target.value)}
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="50"
                  type="text"
                  title="email"
                  name="email"
                  className="form-control"
                  placeholder="Correo electrónico"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-sm-12">
                <input
                  maxlength="100"
                  type="text"
                  title="dni"
                  name="dni"
                  className="form-control"
                  placeholder="DNI"
                  value={dni}
                  disabled
                  onChange={(e) => setDni(e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="modal-footer justify-content-center">
            <button
              type="button"
              className="btn btn-secondary"
              style={{ marginRight: "30px" }}
              data-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              type="button"
              className="btn btn-info"
              data-dismiss="modal"
              style={{ marginLeft: "30px" }}
              onClick={(e) => {
                props.handleEditAuditor(
                  { dni: dni, publicKey: publicKey, email: email },
                  props.positionSelected
                );
              }}
            >
              Guardar cambios
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

function ModalDeleteAuditor(props) {
  return (
    <div
      id="modal-deleteAuditor"
      className="modal fade"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h6 className="modal-title">Dese eliminar el auditor creado?</h6>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <p>Se procederá a eliminar el presente auditor con DNI {" "+props.auditorSelected.dni+"."}</p>
          </div>
          <div className="modal-footer justify-content-between">
            <button
              type="button"
              className="btn btn-secondary"
              style={{ marginRight: "30px" }}
              data-dismiss="modal"
            >
              Cancelar
            </button>
            <button
              type="button"
              className="btn btn-danger"
              data-dismiss="modal"
              onClick={(e) => {
                props.handleDeleteAuditor();
              }}
            >
              Eliminar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

class Settings extends Component {
  constructor(props) {
    super();
    this.state = {
      itemSelected: -1,
      loading: true,
      role: 1, //1:admin 2:auditor
      positionSelected: 0,
      data: [],
      handleChangeSuccesfull_paramEncryp: false,
      emailSent: false,
    };
  }

  test_prime(n) {
    if (n === 1) {
      return false;
    } else if (n === 2) {
      return true;
    } else {
      for (var x = 2; x < n; x++) {
        if (n % x === 0) {
          console.log(n, x);
          return false;
        }
      }
      console.log(n);
      return true;
    }
  }

  handleSave_Q_P_values = async () => {
    console.log(this.props.encryption.q_upper_limit, this.props.encryption.g_generator);
    if (this.props.encryption.q_upper_limit.length === 0 || this.props.encryption.g_generator.length === 0){
      alert("Debe completar los campos de parámetros de encriptación!");
      return;
    }
    if (!this.test_prime(this.props.encryption.q_upper_limit)) {
      alert("El valor del límite del grupo cíclico debe ser primo!");
      this.props.handleChange_Qvalue("");
      this.setState({ handleChangeSuccesfull_paramEncryp: false });
      return;
    } 
    if (!this.test_prime(this.props.encryption.g_generator)) {
      alert("El valor del generador del grupo cíclico debe ser primo!");
      this.props.handleChange_Gvalue("");
      this.setState({ handleChangeSuccesfull_paramEncryp: false });
      return;
    }

    if (parseInt(this.props.encryption.g_generator) > parseInt(this.props.encryption.q_upper_limit)) {
      alert("El valor del generador del grupo cíclico debe ser menor que el límite que el grupo cíclico!");
      this.props.handleChange_Gvalue("");
      this.setState({ handleChangeSuccesfull_paramEncryp: false });
      return;
    }

    
    console.log(this.props.encryption.q_upper_limit, this.props.encryption.g_generator);
    //Llamar al servicio del back
    const contract = this.props.contract;
    const accounts = this.props.accounts;
    try {
      contract.methods
        .save_Q_G_values(
          this.props.encryption.q_upper_limit,
          this.props.encryption.g_generator
        )
        .send({ from: accounts[0] })
        .then(() => {
          this.setState({ handleChangeSuccesfull_paramEncryp: true });
        });
    } catch (err) {
      console.log(err);
    }
  
  };

  handleSaveAuditor = async (dni, publicKey, email) => {
    console.log(dni, publicKey, email);

    //Llamar al servicio y guardar
    if (dni.length === 0 || email.length === 0 || publicKey.length === 0){
      console.log("Completar todos los campos");
      toast.show({title: 'Error', message:'No se agrego ningun auditor', position: 'topright', type: 'warn'});
      return;
    }
    const contract = this.props.contract;
    const accounts = this.props.accounts;
    await contract.methods
      .addNewTallyAuthoritie(dni, publicKey, email)
      .send({ from: accounts[0] });
    this.props.handleAddAuditor(dni, publicKey, email);
  };

  handleEditAuditor = async (auditor, position) => {
    this.props.handleEditAuditor(auditor, position);
    //Llamar al servicio para actualizar
    const contract = this.props.contract;
    const accounts = this.props.accounts;
    await contract.methods
      .updateWalletTallyAuthoritie(
        auditor.dni,
        auditor.publicKey,
        auditor.email
      )
      .send({ from: accounts[0] });
  };

  handleDeleteAuditor = async () => {
    //Llamar al servicio para actualizar
    const contract = this.props.contract;
    const accounts = this.props.accounts;
    await contract.methods
      .deleteWalletTallyAuthoritie(
        this.props.encryption.auditors[this.state.positionSelected].dni
      )
      .send({ from: accounts[0] });
    this.props.handleDeleteAuditor(this.state.positionSelected);
  };

  componentDidMount = async () => {
    try {
      const contract = this.props.contract;
      //q_upper_limit, g_generator
      const q_upper_limit = await contract.methods.q_upper_limit().call();
      const g_generator = await contract.methods.g_generator().call();
      console.log(q_upper_limit, g_generator );
      if (q_upper_limit != 0) {
        this.props.handleChange_Qvalue(q_upper_limit);
        this.props.handleChange_Gvalue(g_generator);
      }
      
      const flagProcessStart = await contract.methods.flagProcessStart().call();

      this.setState({flagStartProcess :flagProcessStart})

      //Setting the state values
      if (this.props.user.role.localeCompare("admin") === 0) {
        //Call all data from blockchain
        const response2 = await contract.methods.getTallyAuthorities().call();
        console.log(response2);
        this.props.handleSetAuditors(response2[0], response2[1], response2[2]);

        const flag = await contract.methods.flagSentEmailTallyAuthorities().call();

        this.setState({ role: 1, emailSent: flag });
      } else {
        const response3 = await contract.methods
          .getTallyAuthoritie(this.props.user.lastName, this.props.accounts[0])
          .call();
        console.log(response3);
        if (response3[1].localeCompare("1") !== 0) {
          this.props.handleChangeAuditorPublicKey(response3[2]);
          this.props.handleChangeAuditorPrivateKey(response3[1]);
        }

        this.setState({ role: 2 });
      }

      this.setState({ loading: false });
    } catch (err) {
      console.log(err);
      this.setState({ loading: false });
    }
  };

  setAuditorSelected = async (index) => {
    console.log(index);
    await this.setState({ positionSelected: index });
    console.log(this.state.positionSelected);
  };

  handleGeneratePublicKey = async () => {
    try {
      if (this.props.encryption.auditor_private_key.length === 0) {
        alert("Debe ingresar un valor en la clave privada!");
      }
    } catch (err) {
      console.log(err);
    }
    if (!this.test_prime(this.props.encryption.auditor_private_key)) {
      alert("El valor de la clave privada debe ser primo!");
      await this.props.handleChangeAuditorPrivateKey("");
    } else {
      const contract = this.props.contract;
      const accounts = this.props.accounts;
      //Call service to generate the public Key
      try {
        console.log(
          this.props.user.lastName,
          this.props.encryption.auditor_private_key
        );
        const response = await contract.methods
          .setPublicKeyAndPrivateKeyEncryption(
            this.props.user.lastName,
            this.props.encryption.auditor_private_key
          )
          .send({ from: accounts[0] });
        console.log(response);
        this.props.handleChangeAuditorPublicKey(
          Math.pow(
            this.props.encryption.g_generator,
            this.props.encryption.auditor_private_key
          )
        );
      } catch (err) {
        console.log(err);
      }
    }
  };

  handleSendEmail = () => {
    const accounts = this.props.accounts;
    const contract = this.props.contract;
    try {
      //Calling the service to send emails
      console.log(this.props.encryption.auditors);
      axios
        .post(
          "http://localhost:8080/auditors/notifyRole",
          this.props.encryption.auditors
        )
        .then((response) => {
          console.log("Emails are sendind ...", response);
          this.setState({ emailsSent: true });
        });
      contract.methods
        .setFlagSentEmailTallyAuthorities(true)
        .send({ from: accounts[0] });
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    console.log(this.props);
    if (this.state.loading) {
      return (
        <div style={{ width: "min-content", margin: "auto", zoom: 3 }}>
          <Spinner animation="border" variant="info" />
        </div>
      );
    }

    if (this.state.role === 1) {
      //Role = 1: Admin
      return (
        <div>
          <ModalAddAuditor handleAddAuditor={this.handleSaveAuditor} />
          <ModalDeleteAuditor 
              handleDeleteAuditor={this.handleDeleteAuditor}  
              auditorSelected={
                this.props.encryption.auditors.length === 0
                  ? { dni: "", publicKey: "", email: "" }
                  : this.props.encryption.auditors[this.state.positionSelected]
              }/>
          <ModalEditAuditor
            handleEditAuditor={this.handleEditAuditor}
            auditorSelected={
              this.props.encryption.auditors.length === 0
                ? { dni: "", publicKey: "", email: "" }
                : this.props.encryption.auditors[this.state.positionSelected]
            }
            positionSelected={this.state.positionSelected}
          />

          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Configuración de seguridad</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item active">
                      Módulo de seguridad
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </section>
          <section style={{ maxWidth: "900px", margin: "auto" }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card card-primary card-outline">
                    <div className="card-header row" style={{ margin: "0px" }}>
                      <div className="col" style={{ paddingTop: "5px" }}>
                        <h3 className="card-title">
                          Parámetros de encriptación
                        </h3>
                      </div>
                      <div className="col" style={{ display: "contents" }}>
                        <span className="align-right">
                          <a
                            style={{
                              cursor: "pointer",
                              lineHeight: "1.5",
                              borderRadius: ".3rem",
                              color: "#fff",
                              backgroundColor: "#007bff",
                              borderColor: "#007bff",
                              boxShadow: "none",
                              border: "1px solid transparent",
                              padding: ".385rem .75rem",
                              display: this.state.flagStartProcess ? 'none':'block'
                            }}
                            title="Guardar parámetros"
                            onClick={() => {
                              this.handleSave_Q_P_values();
                            }}
                          >
                            <i
                              class="fas fa-save"
                              style={{ marginRight: "5px" }}
                            />
                            Guardar
                          </a>
                        </span>
                      </div>
                    </div>
                    <div className="card-body">
                      <form className="form-horizontal">
                        <div className="row">
                          <div className="col-6">
                            <div className="form-group row">
                              <label className="col-sm-6 col-form-label">
                                Límite del grupo cíclico
                              </label>
                              <div className="col-sm-5">
                                <input
                                  maxlength="20"
                                  type="number"
                                  title="variable <q> del algoritmo de encriptación"
                                  name="title"
                                  className="form-control"
                                  placeholder="Número primo ..."
                                  value={this.props.encryption.q_upper_limit}
                                  disabled= {this.state.flagStartProcess}
                                  onChange={(e) =>
                                    this.props.handleChange_Qvalue(
                                      e.target.value
                                    )
                                  }
                                />
                              </div>
                            </div>
                          </div>

                          <div className="col-6">
                            <div className="form-group row">
                              <label className="col-sm-6 col-form-label">
                                Generador del grupo cíclico
                              </label>
                              <div className="col-sm-5">
                                <input
                                  maxlength="5"
                                  type="number"
                                  title="variable <g> del algoritmo de encriptación"
                                  name="title"
                                  className="form-control"
                                  placeholder="Generador..."
                                  value={this.props.encryption.g_generator}
                                  disabled= {this.state.flagStartProcess}
                                  onChange={(e) =>
                                    this.props.handleChange_Gvalue(
                                      e.target.value
                                    )
                                  }
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                      <div
                        className="alert alert-info alert-dismissible fade show"
                        role="alert"
                        style={{
                          color: "#0c5460",
                          backgroundColor: "#d1ecf1",
                          borderColor: "#bee5eb",
                          fontSize: "small",
                          paddingLeft: "15epx",
                          paddingTop: "10px",
                          display: this.state.handleChangeSuccesfull_paramEncryp
                            ? "block"
                            : "none",
                        }}
                      >
                        <div>
                          <strong>Proceso exitoso!</strong> Se han guardado los
                          cambios.
                        </div>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="alert"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <div className="card card-primary card-outline">
                    <div className="card-header row" style={{ margin: "0px" }}>
                      <div className="col" style={{ paddingTop: "5px" }}>
                        <h3 className="card-title">Creación de auditores</h3>
                      </div>
                      <div className="col" style={{ display: this.state.flagStartProcess ? 'none': "contents" }}>
                        <span className="align-right">
                          <a
                            style={{
                              cursor: "pointer",
                              lineHeight: "1.5",
                              borderRadius: ".3rem",
                              color: "#fff",
                              backgroundColor: "#007bff",
                              borderColor: "#007bff",
                              boxShadow: "none",
                              border: "1px solid transparent",
                              padding: ".385rem .75rem",
                            }}
                            title="Guardar parámetros"
                            onClick={() => {
                              this.handleSendEmail();
                            }}
                          >
                            <i
                              class="far fa-envelope"
                              style={{marginRight: "5px" }}
                            />
                            Enviar correo
                          </a>
                        </span>
                      </div>
                    </div>
                    <div className="row">
                      <div className="card-body" style={{ lineHeight: 1.6 }}>
                        <div className="row">
                          <div
                            className="alert alert-info alert-dismissible fade show"
                            role="alert"
                            style={{
                              color: "#0c5460",
                              backgroundColor: "#d1ecf1",
                              borderColor: "#bee5eb",
                              fontSize: "small",
                              paddingLeft: "15px",
                              paddingTop: "10px",
                              marginLeft:'15px',
                              display: this.state.emailsSent ? "block" : "none",
                            }}
                          >
                            <div>
                              <strong>Proceso exitoso!</strong> Los correos ya
                              han sido enviados.
                            </div>
                            <button
                              type="button"
                              className="close"
                              data-dismiss="alert"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                        </div>

                        <div className="row">
                          <button
                            data-toggle="modal"
                            data-target="#modal-addAuditor"
                            style={{ margin: "4px 14px" }}
                            type="button"
                            className="btn btn-primary"
                            disabled= {this.state.flagStartProcess}
                          >
                            <i
                              class="fas fa-plus"
                              style={{ marginRight: "5px" }}
                            />
                            Agregar
                          </button>
                        </div>
                        <div className="row">
                          <div className="col">
                            <div
                              className="card-body table-responsive-md"
                              style={{ overflowX: "scroll" }}
                            >
                              <table className="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">DNI</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Clave pública</th>
                                    <th scope="col">Editar</th>
                                    <th scope="col">Eliminar</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.props.encryption.auditors.map(
                                    (item, index) => {
                                      return (
                                        <tr>
                                          <th scope="row">{index + 1}</th>
                                          <th> {item.dni} </th>
                                          <th> {item.email} </th>
                                          <th> {item.publicKey} </th>
                                          <td>
                                            <button
                                              data-toggle="modal"
                                              data-target="#modal-editAuditor"
                                              style={{ color: "white" }}
                                              type="button"
                                              className="btn btn-warning"
                                              onClick={(e) => {
                                                this.setAuditorSelected(index);
                                              }}
                                            >
                                              <i class="fas fa-edit" />
                                            </button>
                                          </td>
                                          <td>
                                            <button
                                              data-toggle="modal"
                                              data-target="#modal-deleteAuditor"
                                              type="button"
                                              className="btn btn-danger"
                                              onClick={(e) => {
                                                this.setAuditorSelected(index);
                                              }}
                                            >
                                              <i class="fas fa-trash" />
                                            </button>
                                          </td>
                                        </tr>
                                      );
                                    }
                                  )}
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      );
    } else {
      //ROLE= 2 : Auditor
      return (
        <div>
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Configuración de seguridad</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item active">
                      Módulo de seguridad
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </section>
          <section style={{ maxWidth: "900px", margin: "auto" }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card card-primary card-outline">
                    <div className="card-header row" style={{ margin: "0px" }}>
                      <div className="col" style={{ paddingTop: "5px" }}>
                        <h3 className="card-title">
                          Generar claves de encriptación
                        </h3>
                      </div>
                      <div className="col" style={{ display: "contents" }}>
                        <span className="align-right">
                          <a
                            style={{
                              cursor: "pointer",
                              lineHeight: "1.5",
                              borderRadius: ".3rem",
                              color: "#fff",
                              backgroundColor: "#007bff",
                              borderColor: "#007bff",
                              boxShadow: "none",
                              border: "1px solid transparent",
                              padding: ".385rem .75rem",
                              display: this.state.flagStartProcess ? 'none':'block'
                            }}
                            title="Guardar parámetros"
                            onClick={() => {
                              this.handleGeneratePublicKey();
                            }}
                          >
                            <i
                              class="fas fa-save"
                              style={{ marginRight: "5px" }}
                            />
                            Generar clave pública
                          </a>
                        </span>
                      </div>
                    </div>
                    <div className="card-body">
                      <div>
                        <p style={{ color: "gray" }}>
                          Ingresar un número primo menor que
                          <b>
                            {" " + this.props.encryption.q_upper_limit + " "}
                          </b>{" "}
                          como clave privada para la encriptación:
                        </p>
                      </div>
                      <form className="form-horizontal">
                        <div className="row">
                          <div className="col-6">
                            <div className="form-group row">
                              <label className="col-sm-4 col-form-label">
                                Clave privada
                              </label>
                              <div className="col-sm-6">
                                <input
                                  maxlength="5"
                                  type="number"
                                  title="Clave privada para el algoritmo de encriptación"
                                  name="privateKey"
                                  className="form-control"
                                  disabled=  {this.state.flagStartProcess}
                                  placeholder="Clave privada ..."
                                  value={
                                    this.props.encryption.auditor_private_key
                                  }
                                  onChange={(e) =>
                                    this.props.handleChangeAuditorPrivateKey(
                                      e.target.value
                                    )
                                  }
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="form-group row">
                              <label className="col-sm-4 col-form-label">
                                Clave pública
                              </label>
                              <div className="col-sm-6">
                                <input
                                  maxlength="20"
                                  type="number"
                                  title="clave pública para algoritmo de encriptación"
                                  name="publicKey"
                                  className="form-control"
                                  placeholder="Clave pública ..."
                                  value={
                                    this.props.encryption.auditor_public_key
                                  }
                                  disabled
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    encryption: state.encryption,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSetAuditors: (dnis, publicKeys, emails) =>
      dispatch(handleSetAuditors(dnis, publicKeys, emails)),
    handleChange_Qvalue: (value) => dispatch(handleChange_Qvalue(value)),
    handleChange_Gvalue: (value) => dispatch(handleChange_Gvalue(value)),
    handleAddAuditor: (dni, publicKey, email) =>
      dispatch(handleAddAuditor(dni, publicKey, email)),
    handleEditAuditor: (value, position) =>
      dispatch(handleEditAuditor(value, position)),
    handleDeleteAuditor: (position) => dispatch(handleDeleteAuditor(position)),
    handleChangeAuditorPrivateKey: (value) =>
      dispatch(handleChangeAuditorPrivateKey(value)),
    handleChangeAuditorPublicKey: (value) =>
      dispatch(handleChangeAuditorPublicKey(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
