import React, { Component } from 'react';
import { connect } from 'react-redux';
import {handleSaveListVoting,handleCancelListVoting} from '../../../redux/actions/votingListActions';
import Step3 from '../createProcess/Step3';
import ElectoralProcess from '../createProcess/ElectoralProcess';

function isInList (element, list) {
    for (let i = 0;i <list.length;i++){
      if (list[i].value.localeCompare(element.value) === 0) {
        return true;
      }
    }
    return false;
}

class ListVotingEdit extends Component {
    constructor(props) {
        super();
     }
    handleElectoralProcess() {
        console.log("Redireccionando a ElectoralProcess")
        this.props.handleNextComponent(ElectoralProcess);
    }

    handleSaveChanges = async () => {
        const contract = this.props.contract;
        const accounts = this.props.accounts;
        const politicalPartiesList = this.props.politicalPartiesList;
        const votingList = this.props.votingList;

        let isComplete = true;
        for (var index = 0; index < votingList; index++){
            const item = votingList[index];
            if (item.title.length=== 0){
              isComplete=false;
              break;
            }

            if (item.type.length === 0){
              isComplete=false;
              break;
            }
        }
        if (!isComplete) {
            alert("Debe completar todos los campos antes de guardar los cambios");
            return;
        }
        console.log(contract, accounts, politicalPartiesList, votingList);

        let titles = [];
        let descriptions = [];
        let votoTypes = [];
        let keys = [];
        let keyPoliticalParties = [];
        let candidates = [];
        votingList.map((item, index) => {
            titles.push(item.title);
            descriptions.push(item.description);
            votoTypes.push(item.type.value);
            keys.push(index);
            let auxKeyPP = [];
            let auxCand1 = [];
            item.politicalParties.map((politicalPartie, position) => {
                if (politicalPartie.isChecked) {
                auxKeyPP.push(position);
                }else {
                auxKeyPP.push(-1);
                }

                let auxCand2 = [];
                politicalPartie.candidates.map((candidate,pos) => {
                //console.log("candidate", candidate);
                if (isInList(candidate,politicalPartie.selectedCandidates)) {
                    console.log("entro");
                    auxCand2.push(pos);
                }else {
                    auxCand2.push(-1);
                }
                }) 
                auxCand1.push(auxCand2);
            })
            candidates.push(auxCand1);
            keyPoliticalParties.push(auxKeyPP);
        }) 

        console.log(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates);
        //3° service
        contract.methods.addVotingLists(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates).send({ from: accounts[0] }).then(() => {
            this.handleElectoralProcess();
        })
            
    }

    render() {
        const {handleCancelListVoting} = this.props;
        return (
            <div>
                <section className="content-header">
                  <div className="container-fluid">
                      <div className="row mb-2">
                      <div className="col-sm-6">
                        <h1>Editar Lista de cédulas</h1>
                      </div>
                      <div className="col-sm-6">
                          <ol className="breadcrumb float-sm-right">
    
                          <li className="breadcrumb-item">
                              <a style={{cursor: 'pointer',color:'#007bff', textDecoration:"none", backgroundColor:'transparent'}} onClick={() => this.handleElectoralProcess()}>Proceso Electoral</a>
                          </li>
                          <li className="breadcrumb-item active">Editar Lista de cédulas</li>
                          </ol>
                      </div>
                      </div>
                  </div>
                </section>
                <section style={{ maxWidth: "1100px", margin: "auto" }}>
                  <div className="container-fluid">
                  <div className="col-md-12">
                    <div className="row" >
                        <Step3/>
                    </div>
                    <div className="col">
                        <button 
                            className="btn btn-default" 
                            onClick={() => {
                                //handleCancelListVoting();
                                this.handleElectoralProcess();
                            }}
                        > Cancelar</button>
                        <button 
                            className="btn btn-primary  float-right" 
                            onClick={() => {
                                this.handleSaveChanges();
                            }}
                        > Aceptar</button>
                    </div>
      
                  </div>
                  </div>
                </section>
              </div>
              
            )
    }
}

const mapStateToProps = (state) => ({
    politicalPartiesList: state.politicalPartiesList,
    votingList: state.votingList,
})

const mapDispatchToProps = (dispatch) => {
    return {
      handleSaveListVoting: () => dispatch(handleSaveListVoting()),
      handleCancelListVoting: () => dispatch(handleCancelListVoting()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListVotingEdit)
