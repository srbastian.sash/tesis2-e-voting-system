import React, { Component } from 'react';
import { connect } from 'react-redux';
import {handleSavePoliticalParties,handleCancelPoliticalParties} from '../../../redux/actions/politicalPartiesListActions';
import {handleUpdateVotingList} from '../../../redux/actions/votingListActions';
import Step2 from '../createProcess/Step2';
import ElectoralProcess from '../createProcess/ElectoralProcess';

const changeCandidates = "changeCandidates";
function isInList (element, list) {
    for (let i = 0;i <list.length;i++){
      if (list[i].value.localeCompare(element.value) === 0) {
        return true;
      }
    }
    return false;
}

class PoliticalPartiesListEdit extends Component {
    constructor(props) {
        super();
     }
    handleElectoralProcess() {
        console.log("Redireccionando a ElectoralProcess")
        this.props.handleNextComponent(ElectoralProcess);
    }

    handleSaveChanges = async () => {
        const contract = this.props.contract;
        const accounts = this.props.accounts;
        const politicalPartiesList = this.props.politicalPartiesList;
        const votingList = this.props.votingList;
        //Validate all data is completed
        let isComplete = true;

        // console.log(politicalPartiesList)
        if (politicalPartiesList.length === 0){
            alert("Debe crear al menos un partido político antes de guardar los cambios");
            return;
        }
        for (var index = 0; index < politicalPartiesList.length; index++) {
            const partie = politicalPartiesList[index]; 
            // console.log(partie);
            if (partie.name.localeCompare("")=== 0){
            isComplete=false;
            break;
            }
            if (partie.partieLogo.length === 0){
            // console.log(partie.partieLogo);
            isComplete=false;
            break;
            }
            if (partie.candidates.length === 0){
            isComplete=false;
            break;
            }
        }
        if (!isComplete) {
            alert("Debe completar todos los campos antes de ir al siguiente paso");
            return;
        }

        await this.props.handleUpdateVotingList(changeCandidates, politicalPartiesList);
        console.log(contract, accounts, "politicalPartiesList", politicalPartiesList, " votingList", votingList);

        //2° service
        let ids = [];
        let items = [];
        let candidates = []
        politicalPartiesList.map((item, index) => {
            ids.push(index);
            items.push(item.name);
            let aux =  [];
            item.candidates.map((candidate) => {
                aux.push(candidate.value);
            })
            candidates.push(aux);
        });
        console.log(ids,items, candidates);

        contract.methods.addPoliticalParties(ids, items, candidates).send({ from: accounts[0] }).then(() => {

            let titles = [];
            let descriptions = [];
            let votoTypes = [];
            let keys = [];
            let keyPoliticalParties = [];
            let candidates = [];
            votingList.map((item, index) => {
                titles.push(item.title);
                descriptions.push(item.description);
                votoTypes.push(item.type.value);
                keys.push(index);
                let auxKeyPP = [];
                let auxCand1 = [];
                item.politicalParties.map((politicalPartie, position) => {
                    if (politicalPartie.isChecked) {
                        auxKeyPP.push(position);
                    }else {
                        auxKeyPP.push(-1);
                    }

                    let auxCand2 = [];
                    politicalPartie.candidates.map((candidate,pos) => {
                        //console.log("candidate", candidate);
                        if (isInList(candidate,politicalPartie.selectedCandidates)) {
                            console.log("entro");
                            auxCand2.push(pos);
                        }else {
                            auxCand2.push(-1);
                        }
                        console.log("auxCand2", auxCand2)
                    }) 
                    auxCand1.push(auxCand2);
                    console.log("auxCand1", auxCand1)
                })
                candidates.push(auxCand1);
                keyPoliticalParties.push(auxKeyPP);
                }) 
                console.log(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates);
                //3° service
                contract.methods.addVotingLists(keys, titles, keyPoliticalParties, descriptions, votoTypes, candidates).send({ from: accounts[0] }).then(() => {
                    
                    this.handleElectoralProcess();
                })
            }); 
    }

    render() {
        const {handleSavePoliticalParties,handleCancelPoliticalParties} = this.props;
        return (
            <div>
            <section className="content-header">
              <div className="container-fluid">
                  <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1>Editar Lista de Partidos políticos</h1>
                  </div>
                  <div className="col-sm-6">
                      <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                          <a style={{cursor: 'pointer',color:'#007bff', textDecoration:"none", backgroundColor:'transparent'}} onClick={() => this.handleElectoralProcess()}>Proceso Electoral</a>
                      </li>
                      <li className="breadcrumb-item active">Editar Lista de Partidos Políticos</li>
                      </ol>
                  </div>
                  </div>
              </div>
            </section>
            <section style={{ maxWidth: "1100px", margin: "auto" }}>
              <div className="container-fluid">
              <div className="col-md-12">
              <div className="row" >
                    <Step2/>
                </div>
                <div className="col">
                        <button 
                            className="btn btn-default" 
                            onClick={() => {
                                //handleCancelPoliticalParties();
                                this.handleElectoralProcess();
                            }}
                        > Cancelar</button>
                        <button 
                            className="btn btn-primary  float-right" 
                            onClick={() => {
                                this.handleSaveChanges();
                            }}
                        > Aceptar</button>
                    </div>
  
              </div>
              </div>
            </section>
          </div> 
        )
    }
}

const mapStateToProps = (state) => ({
    politicalPartiesList: state.politicalPartiesList,
    votingList: state.votingList,
})

const mapDispatchToProps = (dispatch) => {
    return {
      handleSavePoliticalParties: () => dispatch(handleSavePoliticalParties()),
      handleCancelPoliticalParties: () => dispatch(handleCancelPoliticalParties()),
      handleUpdateVotingList: (name,politicalPartiesList) => dispatch(handleUpdateVotingList(name,politicalPartiesList)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PoliticalPartiesListEdit)
