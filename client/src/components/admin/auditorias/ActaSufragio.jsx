import React, { Component } from 'react';
import DataTable from './DataTable';
export default class ActaSufragio extends Component {
    render() {
        return (
        <div>
        <section className="content-header">
            <div className="container-fluid">
                <div className="row mb-2">
                <div className="col-sm-6">
                <h1>Auditoría sobre el sufragio</h1>
                </div>
                <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                    
                    <li className="breadcrumb-item active">Acta de sufragio</li>
                    </ol>
                </div>
                </div>
            </div>
        </section>

        <section style={{ maxWidth: "900px", margin: "auto" }}>
            <div className="container-fluid">
                <div className="col-md-14">
                    <div className="row" style={{margin:'20px'}} >
                    <div className="card card-primary card-outline" >
                        <div className="card-header">
                            <h3 className="card-title"><b>Acta de sufragio</b></h3>
                        </div>
                        <div className="card-body" style={{lineHeight:1.6}}>
                        <DataTable />
                            <div className="form-group row">
                                <label className="col-sm-3 col-fo rm-label">
                                Observación
                                </label>
                                <div className="col-sm-12">
                                <textarea
                                    class="form-control"
                                    rows="3"
                                    name="description"
                                    placeholder="Escribir alguna observación sobre el proceso electoral ..."
                                ></textarea>
                                </div>
                            </div>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            
        </div>
    )
    }
}
