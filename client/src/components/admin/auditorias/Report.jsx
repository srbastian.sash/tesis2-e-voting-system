import React, { Component } from "react";
import { connect } from "react-redux";
import { MDBDataTableV5 } from "mdbreact";
import SimplePie from "../graphics/SimplePie";
import DinamicPie from "../graphics/DinamicPie";
import BarGraphic from "../graphics/BarGraphic";
import Spinner from "react-bootstrap/Spinner";

export class Report extends Component {
  constructor(props) {
    super();
    this.state = {
      flag: false,
      data: {
        columns: [
          {
            label: "Partido politico",
            field: "politicalPartie",
            width: "110",
            attributes: {
              "aria-controls": "DataTable",
              "aria-label": "Name",
            },
          },
          {
            label: "Votos",
            field: "votes",
            sort: "asc",
            width: "130",
          },
        ],
        rows: [
          {
            politicalPartie: "Partido Político 1",
            votes: "30",
          },
          {
            politicalPartie: "Partido Político 2",
            votes: "20",
          },
          {
            politicalPartie: "Partido Político 3",
            votes: "10",
          },
          {
            politicalPartie: "Partido Político 4",
            votes: "40",
          },
        ],
      },
      loading: true,
    };
  }

  signTallying = () => {
    console.log("Firmar proceso de escrutinio y desencriptación");
  };

  componentDidMount = async () => {
    const contract = this.props.contract;
    const flagProcessStart = await contract.methods.flagProcessStart().call();

    this.setState({ flagStartProcess: flagProcessStart });
    try {
      //Setting the state values
      if (this.props.user.role.localeCompare("admin") === 0) {
        //Call all data from blockchain
        const response2 = await contract.methods.getTallyAuthorities().call();
        console.log(response2);
        if (response2[0] > 0) {
          this.props.handleSetAuditors(
            response2[0],
            response2[1],
            response2[2]
          );
        }
        this.setState({ role: 1 });
      } else {
        const response3 = await contract.methods
          .getTallyAuthoritie(this.props.user.lastName, this.props.accounts[0])
          .call();
        console.log(response3);
        if (response3[1].localeCompare("1") !== 0) {
          this.props.handleChangeAuditorPublicKey(response3[2]);
          this.props.handleChangeAuditorPrivateKey(response3[1]);
        }

        this.setState({ role: 2 });
      }

      this.setState({ loading: false });
    } catch (err) {
      console.log("error");
      alert("Ha sucedido un error, vuelva a cargar la página!");
    }
  };
  render() {
    if (this.state.loading) {
      return (
        <div style={{ width: "min-content", margin: "auto", zoom: 3 }}>
          <Spinner animation="border" variant="info" />
        </div>
      );
    }
    if (!this.state.flagStartProcess) {
      if (this.state.role === 1) {
        //Role = 1: Admin
        return (
          <div>
            <section className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-9">
                    <h1>
                      {" "}
                      Firmar proceso empezar para realizar el proceso de
                      escrutinio y desencriptación final
                    </h1>
                  </div>
                  <div className="col-sm-3">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <a>Reporte</a>
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </section>
            <div
              style={{
                width: "fit-content",
                margin: "40px auto",
              }}
            >
              <button
                className="btn btn-primary btn-lg"
                onClick={this.signTallying}
              >
                {" "}
                Firmar proceso
              </button>
            </div>

            <section></section>
          </div>
        );
      } else {
        //ROLE= 2 : Auditor
        return <div>Soy audiotr</div>;
      }
    } else {
      return (
        <div>
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Reportes del proceso electoral </h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item active">Reportes </li>
                  </ol>
                </div>
              </div>
            </div>
          </section>

          {this.state.flag ? (
            <div>No se ha generado ningún reporte</div>
          ) : (
            <div>
              <section style={{ maxWidth: "900px", margin: "auto" }}>
                <div className="container-fluid">
                  <div className="col-md-12">
                    <div className="row" style={{ paddingBottom: "20px" }}>
                      <div
                        className="callout callout-info"
                        style={{ width: "100%", margin: "auto" }}
                      >
                        <h3>Resultado de votos emitidos</h3>
                        <h6>Total de votos: 152</h6>
                        <p style={{ marginBottom: "0px", paddingLeft: "20px" }}>
                          Votos validos: 26
                        </p>
                        <p style={{ marginBottom: "0px", paddingLeft: "20px" }}>
                          Votos en blanco: 126
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <div className="row" style={{ margin: "18px" }}>
                <div className="col-md-12">
                  <div className="card card-primary card-outline">
                    <div className="card-header">
                      <h3 className="card-title">
                        <b>Estadisticos</b>
                      </h3>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-12 col-md-6">
                          <h4>Votos para presidentes</h4>
                          <div className="card-body">
                            <ul
                              className="nav nav-tabs"
                              id="custom-content-above-tab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="custom-content-above-graphicsPresident-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-graphicsPresident"
                                  role="tab"
                                  aria-controls="custom-content-above-graphicsPresident"
                                  aria-selected="true"
                                >
                                  Gráfico{" "}
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="custom-content-above-tablePresident-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-tablePresident"
                                  role="tab"
                                  aria-controls="custom-content-above-tablePresident"
                                  aria-selected="false"
                                >
                                  Tabla{" "}
                                </a>
                              </li>
                            </ul>
                            <div
                              className="tab-content"
                              id="custom-content-above-tabContent"
                            >
                              <div
                                className="tab-pane fade show active"
                                id="custom-content-above-graphicsPresident"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-graphicsPresident-tab"
                              >
                                <SimplePie title="Gráfico de Votos para presidente" />
                              </div>
                              <div
                                className="tab-pane fade"
                                id="custom-content-above-tablePresident"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-tablePresident-tab"
                              >
                                <MDBDataTableV5
                                  hover
                                  entriesOptions={[5, 20, 25]}
                                  entries={5}
                                  pagesAmount={4}
                                  data={this.state.data}
                                  searchTop={false}
                                  searchBottom
                                  infoLabel={[
                                    "Mostrando",
                                    "al",
                                    "de",
                                    "entradas",
                                  ]}
                                  entriesLabel=" Cant."
                                  paginationLabel={["Prev", "Sigui"]}
                                  responsive
                                  searchLabel="Buscar"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 col-md-6">
                          <h4>Votos para vicepresidente</h4>
                          <div className="card-body">
                            <ul
                              className="nav nav-tabs"
                              id="custom-content-above-tab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="custom-content-above-graphicsVicePresident-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-graphicsVicePresident"
                                  role="tab"
                                  aria-controls="custom-content-above-graphicsVicePresident"
                                  aria-selected="true"
                                >
                                  Gráfico{" "}
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="custom-content-above-tableVicePresident-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-tableVicePresident"
                                  role="tab"
                                  aria-controls="custom-content-above-tableVicePresident"
                                  aria-selected="false"
                                >
                                  Tabla{" "}
                                </a>
                              </li>
                            </ul>
                            <div
                              className="tab-content"
                              id="custom-content-above-tabContent"
                            >
                              <div
                                className="tab-pane fade show active"
                                id="custom-content-above-graphicsVicePresident"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-graphicsVicePresident-tab"
                              >
                                <DinamicPie title="Gráfica de votos para vicepresidentes" />
                              </div>
                              <div
                                className="tab-pane fade"
                                id="custom-content-above-tableVicePresident"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-tableVicePresident-tab"
                              >
                                <MDBDataTableV5
                                  hover
                                  entriesOptions={[5, 20, 25]}
                                  entries={5}
                                  pagesAmount={4}
                                  data={this.state.data}
                                  searchTop={false}
                                  searchBottom
                                  infoLabel={[
                                    "Mostrando",
                                    "al",
                                    "de",
                                    "entradas",
                                  ]}
                                  entriesLabel=" Cant."
                                  paginationLabel={["Prev", "Sigui"]}
                                  responsive
                                  searchLabel="Buscar"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-md-6">
                          <h4>Votos para congresistas</h4>
                          <div className="card-body">
                            <ul
                              className="nav nav-tabs"
                              id="custom-content-above-tab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="custom-content-above-graphicsCongresistas-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-graphicsCongresistas"
                                  role="tab"
                                  aria-controls="custom-content-above-graphicsCongresistas"
                                  aria-selected="true"
                                >
                                  Gráfico{" "}
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="custom-content-above-tableCongresistas-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-tableCongresistas"
                                  role="tab"
                                  aria-controls="custom-content-above-tableCongresistas"
                                  aria-selected="false"
                                >
                                  Tabla{" "}
                                </a>
                              </li>
                            </ul>
                            <div
                              className="tab-content"
                              id="custom-content-above-tabContent"
                            >
                              <div
                                className="tab-pane fade show active"
                                id="custom-content-above-graphicsCongresistas"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-graphicsCongresistas-tab"
                              >
                                <BarGraphic title="Gráfico de barras para congresistas" />
                              </div>
                              <div
                                className="tab-pane fade"
                                id="custom-content-above-tableCongresistas"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-tableCongresistas-tab"
                              >
                                <MDBDataTableV5
                                  hover
                                  entriesOptions={[5, 20, 25]}
                                  entries={5}
                                  pagesAmount={4}
                                  data={this.state.data}
                                  searchTop={false}
                                  searchBottom
                                  infoLabel={[
                                    "Mostrando",
                                    "al",
                                    "de",
                                    "entradas",
                                  ]}
                                  entriesLabel=" Cant."
                                  paginationLabel={["Prev", "Sigui"]}
                                  responsive
                                  searchLabel="Buscar"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 col-md-6">
                          <h4>Votos para parlamento Andino</h4>
                          <div className="card-body">
                            <ul
                              className="nav nav-tabs"
                              id="custom-content-above-tab"
                              role="tablist"
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link active"
                                  id="custom-content-above-graphicsParlamentoAndino-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-graphicsParlamentoAndino"
                                  role="tab"
                                  aria-controls="custom-content-above-graphicsParlamentoAndino"
                                  aria-selected="true"
                                >
                                  Gráfico{" "}
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link"
                                  id="custom-content-above-tableParlamentoAndino-tab"
                                  data-toggle="pill"
                                  href="#custom-content-above-tableParlamentoAndino"
                                  role="tab"
                                  aria-controls="custom-content-above-tableParlamentoAndino"
                                  aria-selected="false"
                                >
                                  Tabla{" "}
                                </a>
                              </li>
                            </ul>
                            <div
                              className="tab-content"
                              id="custom-content-above-tabContent"
                            >
                              <div
                                className="tab-pane fade show active"
                                id="custom-content-above-graphicsParlamentoAndino"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-graphicsParlamentoAndino-tab"
                              >
                                <BarGraphic />
                              </div>
                              <div
                                className="tab-pane fade"
                                id="custom-content-above-tableParlamentoAndino"
                                role="tabpanel"
                                aria-labelledby="custom-content-above-tableParlamentoAndino-tab"
                              >
                                <MDBDataTableV5
                                  hover
                                  entriesOptions={[5, 20, 25]}
                                  entries={5}
                                  pagesAmount={4}
                                  data={this.state.data}
                                  searchTop={false}
                                  searchBottom
                                  infoLabel={[
                                    "Mostrando",
                                    "al",
                                    "de",
                                    "entradas",
                                  ]}
                                  entriesLabel=" Cant."
                                  paginationLabel={["Prev", "Sigui"]}
                                  responsive
                                  searchLabel="Buscar"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* /.card */}
                  </div>
                </div>
                {/* /.col */}
              </div>
            </div>
          )}
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Report);
