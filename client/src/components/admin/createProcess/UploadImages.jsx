import React, { Component } from 'react';
import ImageUploader from 'react-images-upload';
import { S3FileUpload ,uploadFile, deleteFile  } from 'react-s3';
import {aws } from '../../../keys';
import AWS from 'aws-sdk';
import { connect } from 'react-redux';

AWS.config.update({
    accessKeyId: aws.accessKeyId,
    secretAccessKey: aws.secretAccessKey,
  })

const config = {
  bucketName: 'partidospoliticos',
  //dirName: 'logos', /* optional */
  region: 'us-east-1',
  accessKeyId: aws.accessKeyId,
  secretAccessKey: aws.secretAccessKey,
}


class UploadImages extends Component {
    constructor(props) {
        super();
         this.state = { pictures: [] };
         this.myBucket = new AWS.S3({
            params: { Bucket: 'partidospoliticos'},
            region: 'us-east-1',
          })
    }

    uploadFile = (file) => {
        const params = {
          ACL: 'public-read',
          Key: file.name,
          ContentType: file.type,
          Body: file,
        }
        console.log(params);

        this.myBucket.putObject(params)
          .on('httpUploadProgress', (evt) => {
            // that's how you can keep track of your upload progress
            this.setState({
              progress: Math.round((evt.loaded / evt.total) * 100),
            })
            console.log(evt)
          })
          .send((err) => {
             if (err) {
               // handle the error here
               console.log(err)
             }
          })
      }
 
    
    onDrop = async (picture) => {
        console.log(picture);
        let aux = [];
        aux.push(picture);
        await this.setState({
            pictures: aux,
        });
        console.log(this.state.pictures);
        if (picture.length > 0){
            console.log(picture)
            //this.uploadFile(picture[0]);
            this.uploadImages(picture);
        }

        console.log("s",this.state.pictures[0][0].webkitRelativePath);
        
        //this.uploadImages();
    }
    uploadImages = (_file) => {

        console.log(this.state.pictures[0]);
        if (this.state.pictures[0].length > 0){
            console.log(this.state.pictures[0]);
            uploadFile(_file, config)
            .then((data) => {
                console.log(data);
            })
            .catch((err) => {
                console.error(err);
            })
            console.log("si hay acrhivo pa subir")
        }else {
            console.log("No hay archivo pa subir")
        }
    }

    render() {
      
        return (
            <ImageUploader
                withIcon={true}
                buttonText='Agregar logo'
                buttonStyles={{background:"rgb(23,161,183)"}}
                onChange={this.onDrop}
                imgExtension={['.jpg', '.png', '.gif']}
                maxFileSize={1048576}
                withPreview={true}
                label={'Tamaño maximo: 1MB'}
                singleImage={true}
                fileTypeError={"El tipo de archivo no es soportado"}
                fileSizeError={"Tamaño de archivo no soportado"}
            />
        )
    }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadImages)
