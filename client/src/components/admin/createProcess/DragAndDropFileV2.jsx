import React, {Component, useEffect,useMemo, useState } from 'react';
import {useDropzone} from 'react-dropzone';
import {connect} from 'react-redux';
import { handleLogo} from '../../../redux/actions/politicalPartiesListActions';


const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
  margin:'auto',
  textAlign: 'center'
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginRight: 8,
  width: 150,
  height: 150,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
  textAlign: 'center'
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out'
};

const activeStyle = {
  borderColor: '#2196f3'
};

const acceptStyle = {
  borderColor: '#00e676'
};

const rejectStyle = {
  borderColor: '#ff1744'
};

class DragDropFileV2 extends Component {
    constructor(props) {
        super();
    }

    handleLogo = (acceptedFiles) => {
        this.props.handleLogo(acceptedFiles, this.props.index);
    }
    render(){
        const {politicalPartiesList, index} = this.props;
        return(
            <ManageFile 
                file = {politicalPartiesList[index].partieLogo}
                textButton="Agregar logo"
                alert="Arrastrar solo archivos de tipo imagen"  
                handleLogo = {this.handleLogo}
            />
        );
    }
}
function ManageFile(props) {
    const [files, setFiles] = useState(props.file);
  
    useEffect(() => () => {
        setFiles(props.file);
        console.log(props);
    });

    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject,
        acceptedFiles,
        open
    } = useDropzone({
        noClick: true,
        noKeyboard: true,
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setFiles(acceptedFiles.map(file => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));

            //Save the image in S3
            // ReactS3.upload(acceptedFiles[0],config)
            // .then((data) => {
            //   console.log(data);
            // })
            // .catch((err)=> {
            //   alert(err);
            // })

            console.log(acceptedFiles);
            props.handleLogo(acceptedFiles);
        }
    });
  
    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    const nameFile = files.map(file => (
        <p key={file.path} style={{padding:'10px'}}>
        <b>Archivo: &nbsp;</b> {file.path} - {file.size} bytes
        </p>
    ));


    const thumbs = files.map(file =>(
        <div style={thumb} key={file.name}>
            <div style={thumbInner}>
              <img
                src={file.preview}
                style={img}
              />
            </div>
        </div>
    ));

    useEffect(() => () => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    return (
        <div className="container" style={{padding:'0px'}}>
            <div {...getRootProps({style})}>
                <input {...getInputProps()} />
                <p>{props.alert}</p>
                <button type="button" className="btn btn-block btn-info" onClick={open}>
                {props.textButton}
                </button>
            </div>
            <aside>
                <div>
                {nameFile}
                </div>    
            </aside>
            <aside style={thumbsContainer}>
                {thumbs}
            </aside>
        </div>
    );  
}

const mapStateToProps = (state) => {
    return {
      politicalPartiesList: state.politicalPartiesList,
    }    
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      handleLogo: (item,index) => dispatch(handleLogo(item,index)),
    };
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(DragDropFileV2);


