import React, {useEffect,useMemo, useState } from 'react';
import {useDropzone} from 'react-dropzone';
import S3FileUpload from 'react-s3';
import {aws } from '../../../keys';

const config = {
  bucketName: 'partidospoliticos2',
  //dirName: 'logos', /* optional */
  region: 'us-east-1',
  accessKeyId: aws.accessKeyId,
  secretAccessKey: aws.secretAccessKey,
}

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
  margin:'auto',
  textAlign: 'center'
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginRight: 8,
  maxWidth: 200,
  width:'100%',
  maxHeight: 200,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
  textAlign: 'center'
};

const img = {
  display: 'block',
  width: '-webkit-fill-available',
  height: '100%'
};

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out'
};

const activeStyle = {
  borderColor: '#2196f3'
};

const acceptStyle = {
  borderColor: '#00e676'
};

const rejectStyle = {
  borderColor: '#ff1744'
};

export default function DragDropImage(props) {
  const [files, setFiles] = useState([]);
  console.log(props);
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    acceptedFiles,
    open
  } = useDropzone({
    noClick: true,
    noKeyboard: true,
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })));

      console.log(acceptedFiles);
      props.handleLogo(acceptedFiles,props.index);

      //Call AWS Servic
      console.log(acceptedFiles[0]);
      S3FileUpload
      .uploadFile(acceptedFiles[0], config)
      .then(data => console.log(data))
      .catch(err => console.error(err));
      
    }
  });
  
  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isDragActive,
    isDragReject,
    isDragAccept
  ]);
  const nameFile = files.map(file => (
      <p key={file.path} style={{padding:'10px'}}>
       <b>Archivo: &nbsp;</b> {file.path} - {Math.round(file.size/1000)} KB
      </p>
  ));

  const thumbs = files.map(file => 
    {
      return (
        <div style={thumb} key={file.name}>    
            <div style={thumbInner}>
              <img
                src={file.preview}
                style={img}
              />
            </div>
        </div>
      )
    });

  useEffect(() => () => {
    // Make sure to revoke the data uris to avoid memory leaks
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);

  return (
    <div className="container" style={{padding:'0px'}}>
      <div {...getRootProps({style})}>
        <input {...getInputProps()} />
        <p>{props.alert}</p>
        <button type="button" className="btn btn-block btn-info" onClick={open}>
         {props.textButton}
        </button>
      </div>
      <aside>
        <div>
          {nameFile}
        </div>    
      </aside>
      <aside style={thumbsContainer}>
          {thumbs}
      </aside>
    </div>
  );
}


const style_fileRemove = {
  width: '30px',
  height: '30px',
  borderRadius: '50%',
  display: 'block',
  position: 'absolute',
  background: '#aaa',
  lineHeight: '30px',
  color: '#fff',
  fontSize: '12px',
  cursor: 'pointer',
  right: '-15px',
  top: '-15px',
}


