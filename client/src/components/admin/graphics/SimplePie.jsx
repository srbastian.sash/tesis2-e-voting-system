import React, { Component } from 'react';
import CanvasJSReact from '../../../assets/canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class SimplePie extends Component {
	constructor(props) {
        super();
        this.state = {}
    }
    render() {
		const options = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: this.props.title ? this.props.title: "Gráfico de pie"
			},
			data: [{
				type: "pie",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: {y}%",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 16,
				indexLabel: "{label} - {y}%",
				dataPoints: [
					{ y: 18, label: "Partido El Buen sembrado" },
					{ y: 49, label: "Partido Vamos Perú" },
					{ y: 9, label: "Partido Si podemos" },
					{ y: 5, label: "Partido La Casa" },
					{ y: 19, label: "Partido Socialista" }
				]
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				/* onRef={ref => this.chart = ref} */
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
}
