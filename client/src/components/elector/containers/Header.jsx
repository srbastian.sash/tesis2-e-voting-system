import React, { Component } from "react";
import VotationList from "../voteCasting/VotationList";
import Cookies from "universal-cookie";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import { connect } from "react-redux";
import IndividualVerification from '../voteCasting/IndividualVerification';

const cookies = new Cookies();

class MainPage extends Component {
  handleClickCastingVote = async () => {
    const contract = this.props.contract;
    
    const flagProcessStart = await contract.methods.flagProcessStart().call();
    const dateIni = await contract.methods.dateIni().call();

    if (flagProcessStart) {
      if (this.props.flagReadInstructions) {
        const contract = this.props.contract;
        const elector = cookies.get("elector");
        console.log("elector", elector);
        if (elector.castVote) {
          this.props.handleNextComponent(IndividualVerification);
        }
        else {
          console.log("redirecciona a página de emision de votos");
          this.props.handleNextComponent(VotationList);
        }
      } else {
        alert("Debe primero ir a la seccion de Ver instrucciones");
      }
    } else {
      alert("Todavia no empiezan las elecciones. Esperar hasta " + dateIni);
    }
  };
  handleReadInstructions() {
    this.props.handleFlagReadInstructions();
  }


  render() {
    console.log(this.props, this.state);
    const { electoralProcess, politicalPartiesList, votingList } = this.props;
    return (
      <div>
        <header
          className="bgimg-1 w3-display-container w3-grayscale-min"
          id="home"
        >
          <div
            className="w3-display-left w3-text-white"
            style={{ padding: 48 }}
          >
            <span className="w3-jumbo w3-hide-small">
              Sistema de voto electrónico BlockVoting
            </span>
            <br />
            <span className="w3-xxlarge w3-hide-large w3-medium">
              Sistema de voto electrónico basado en blockchain
            </span>
            <br />
            <p>
              <a
                onClick={() => this.handleClickCastingVote()}
                className="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-hover-opacity-off"
                style={{ opacity: "0.9" }}
              >
                Emitir voto
              </a>
            </p>
            <p>
              <a
                href="#instructions"
                onClick={() => this.handleReadInstructions()}
                className="w3-button w3-black"
                style={{ marginTop: "50px", padding: "10px", fontSize: "22px" }}
                autofocus
              >
                <i class="fa fa-arrow-down">&nbsp;</i> Ver instrucciones
              </a>
            </p>
          </div>
        </header>

        <div
          className="w3-container "
          style={{ padding: "70px 16px", backgroundColor: "#ffff" }}
          id="instructions"
        >
          <div className="w3-row-padding">
            <div className="w3-col m6">
              <h2>
                <b>¿Cómo votar?</b>
              </h2>
              <p>
                Puedes emitir tu voto haciendo click en el botón de{" "}
                <b>Emitir voto</b>.
              </p>
            </div>
            <div className="w3-col m6 w3-margin-bottom">
              <div className="w3-card">
                <div className="w3-container">
                  <h3>Instrucciones:</h3>
                  <p>
                    <ul style={{ paddingLeft: "30px" }}>
                      <li>
                        {" "}
                        Puedes editar tu voto en todo momento antes de emitirlo
                        en la urna electrónica{" "}
                      </li>
                      <li>
                        {" "}
                        Si deja la cédula en blanco se considerará como voto en
                        blanco{" "}
                      </li>
                      <li>
                        {" "}
                        Mientras no haya emitido su voto puede entrar al sistema
                        las veces que desee para modificar su voto{" "}
                      </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* PoliticalParties Section */}
        <div
          className="w3-container  w3-light-grey"
          style={{ padding: "70px 16px" }}
          id="politicalParties"
        >
          <h2 className="w3-center">
            <b>Lista de partido políticos</b>
          </h2>
          <div
            className="w3-row-padding w3-grayscale"
            style={{ marginTop: 64 }}
          >
            <Accordion>
              {politicalPartiesList.map((item, index) => {
                console.log(item, index);
                return (
                  <Card>
                    <Accordion.Toggle
                      as={Card.Header}
                      eventKey={index + 1}
                      style={{ cursor: "pointer" }}
                    >
                      {item.name}
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey={index + 1}>
                      <Card.Body>
                        <ol style={{ marginLeft: "15px" }}>
                          {item.candidates.map((candidate) => {
                            return <li> {candidate.value}</li>;
                          })}
                        </ol>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                );
              })}
            </Accordion>
          </div>
          <a
            href="#home"
            class="w3-button w3-black"
            style={{ marginTop: "30px" }}
            onClick={() => this.handleReadInstructions()}
          >
            <i class="fa fa-arrow-up w3-margin-right"></i>Ir arriba
          </a>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    politicalPartiesList: state.politicalPartiesList,
    electoralProcess: state.electoralProcess,
    votingList: state.votingList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
