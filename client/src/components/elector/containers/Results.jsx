import React, { Component } from 'react'

export default class Results extends Component {
    render() {
        return (
            <div className="w3-containe w3-padding-64"  style={{ backgroundColor:"#ffff"}}>
                <div className="w3-row-padding">
                    <div className="w3-col m6">
                    <h3>Resultados</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br />
                        tempor incididunt ut labore et dolore.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br />
                        tempor incididunt ut labore et dolore.</p>
                    </div>
                    <div className="w3-col m6">
                    <p className="w3-wide">Partido político La siembra</p>
                    <div className="w3-grey">
                        <div className="w3-container w3-dark-grey w3-center" style={{width: '70%'}}>70%</div>
                    </div>
                    <p className="w3-wide">Partido político Frente Amplio</p>
                    <div className="w3-grey">
                        <div className="w3-container w3-dark-grey w3-center" style={{width: '25%'}}>25%</div>
                    </div>
                    <p className="w3-wide">Partido político la Marinera</p>
                    <div className="w3-grey">
                        <div className="w3-container w3-dark-grey w3-center" style={{width: '5%'}}>5%</div>
                    </div>
                    </div>
                </div>
            </div>

        )
    }
}
