import React, { Component } from "react";
import VotationList from "../voteCasting/VotationList";
import Results from "../containers/Results";
import Cookies from "universal-cookie";
import Header from "../containers/Header";
import IndividualVerification from '../voteCasting/IndividualVerification';
const cookies = new Cookies();

export default class Navbar extends Component {
  constructor(props) {
    super();
    this.state = {};
  }

  w3_open = (evt) => {
    evt.preventDefault();
    console.log("holi");
    if (document.getElementById("mySidebar").style.display === "block") {
      document.getElementById("mySidebar").style.display = "none";
    } else {
      document.getElementById("mySidebar").style.display = "block";
    }
  };

  w3_close() {
    document.getElementById("mySidebar").style.display = "none";
  }

  handleOpen = (evt) => {
    evt.preventDefault();
  };
  handleClose = (evt) => {
    evt.preventDefault();
    document.getElementById("mySidebar").style.display = "none";
    this.props.handleLogOut();
  };

  handleClickCastingVote = async () => {
    this.w3_close();

    const contract = this.props.contract;
    const flagProcessStart = await contract.methods.flagProcessStart().call();
    const dateIni = await contract.methods.dateIni().call();

    if (flagProcessStart) {
      if (this.props.flagReadInstructions) {
        
        console.log("redirecciona a página de emision de votos");
        const elector = cookies.get("elector");
        console.log("elector", elector);
        if (elector.castVote) {
          this.props.handleNextComponent(IndividualVerification);
        }
        else {
          this.props.handleNextComponent(VotationList);  
        }
      } else {
        alert("Debe primero ir a la seccion de Ver instrucciones");
      }
    } else {
      alert("Todavia no empiezan las elecciones. Esperar hasta " + dateIni);
    }
  };

  handleClickResults() {
    this.w3_close();
    console.log("redirecciona a página de resultados");
    this.props.handleNextComponent(Results);
  }

  handleClickHome() {
    this.w3_close();
    console.log("redirecciona a página de resultados");
    this.props.handleNextComponent(Header);
  }

  render() {
    return (
      <div>
        {/* Navbar (sit on top) */}
        <div className="w3-top">
          <div className="w3-bar w3-white w3-card" id="myNavbar">
            <a
              onClick={(e) => this.handleClickHome(e)}
              className="w3-bar-item w3-button w3-wide"
            >
              <img
                src="images/icono.png"
                alt="AdminLTE Logo"
                className="brand-logo"
                style={{
                  opacity: ".8",
                  marginLeft: "6px",
                  marginRight: "6px",
                  maxHeight: "33px",
                }}
              />
              <span className="brand-text font-weight-light">
                <b>BlockVoting</b>
              </span>
            </a>
            {/* Right-sided navbar links */}
            <div className="w3-right w3-hide-small">
              <a
                onClick={(e) => this.handleClickCastingVote(e)}
                className="w3-bar-item w3-button"
                selected
              >
                Emitir voto
              </a>
              <a
                onClick={(e) => this.handleClickResults(e)}
                className="w3-bar-item w3-button"
              >
                Resultados
              </a>
              <a
                onClick={(e) => this.handleClose(e)}
                className="w3-bar-item w3-button"
              >
                {" "}
                Cerrar sesión
              </a>
            </div>
            {/* Hide right-floated links on small screens and replace them with a menu icon */}
            <a
              className="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium"
              onClick={(e) => this.w3_open(e)}
            >
              <i className="fa fa-bars" />
            </a>
          </div>
        </div>
        {/* Sidebar on small screens when clicking the menu icon */}
        <nav
          className="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large"
          style={{ display: "none" }}
          id="mySidebar"
        >
          <a
            href="javascript:void(0)"
            onClick={(e) => this.w3_close(e)}
            className="w3-bar-item w3-button w3-large w3-padding-16"
          >
            Cerrar ×
          </a>
          <a
            onClick={(e) => this.handleClickCastingVote(e)}
            className="w3-bar-item w3-button"
          >
            Emitir voto
          </a>
          <a
            onClick={(e) => this.handleClickResults(e)}
            className="w3-bar-item w3-button"
          >
            Resultados
          </a>
          <a
            onClick={(e) => this.handleClose(e)}
            className="w3-bar-item w3-button"
          >
            Cerrar sesión
          </a>
        </nav>
      </div>
    );
  }
}
