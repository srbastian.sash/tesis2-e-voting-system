import React, { Component } from "react";
import Navbar from "./Navbar";
import Footer from "../Footer";
import Header from "../containers/Header";
import Cookies from "universal-cookie";
import Election from "../../../contracts/Election.json";
import getWeb3 from "../../../getWeb3";
import { handleSaveListVoting } from "../../../redux/actions/votingListActions";
import { handleSaveElectoralProcess } from "../../../redux/actions/electoralProcessActions";
import { handleSavePoliticalParties } from "../../../redux/actions/politicalPartiesListActions";
import { connect } from "react-redux";
import IndividualVerification from '../voteCasting/IndividualVerification';
const cookies = new Cookies();

class HomeElector extends Component {
  constructor(props) {
    super();
    this.state = {
      workingSpace: Header,
      //workingSpace: IndividualVerification,
      flagReadInstructions: false,
      web3: null,
      accounts: null,
      contract: null,
      loading: true,
    };
  }

  handleLogOut = async () => {
    console.log("Cerrando sesion...");

    await cookies.remove("elector", { path: "/elector" });
    cookies.remove("contract", { path: "/elector" });
    cookies.remove("web3", { path: "/elector" });
    const aux = cookies.get("elector", { path: "/elector" });

    window.location.href = "./login-elector";
  };
  handleNextComponent = (_nextChildComponent) => {
    this.setState({
      workingSpace: _nextChildComponent,
    });
  };
  handleFlagReadInstructions = () => {
    this.setState({
      flagReadInstructions: true,
    });
  };
  componentDidMount = async () => {
    const elector = cookies.get("elector");
    console.log("elector", elector);
    
    try {
      if (elector.DNI.length > 0) {
        this.setState({ elector: elector });
      } else {
        //If I'm not logged
        this.handleLogOut();
        window.location.href = "./login-elector";
      }
    } catch (err) {
      console.log(err);
      this.handleLogOut();
      window.location.href = "./login-elector";
    }

    //Set states
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();
      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Election.networks[networkId];
      const instance = new web3.eth.Contract(
        Election.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      await this.setState({ web3, accounts, contract: instance });
      console.log(this.state, this.props);
      console.log(this.state.contract.methods);
      //Service for general Information
      
      const title = await this.state.contract.methods.title().call();
      const description = await this.state.contract.methods.description().call();
      const dateIni = await this.state.contract.methods.dateIni().call();
      const dateEnd = await this.state.contract.methods.dateEnd().call();

      if (title.length !== 0) {
        //Setting the state
        const stateGeneralInformation = {
          title: title,
          description: description,
          dateIni: dateIni,
          dateEnd: dateEnd,
          types: [
            { value: "unico", label: "Único candidato" },
            { value: "multiple", label: "Múltiples candidatos" },
          ],
          created: true,
        };

        //Service for political Parties
        const response2 = await this.state.contract.methods
          .getPoliticalParties()
          .call();
        const totalPoliticalParties = response2[0].length;
        let statePoliticalParties = [];
        for (let pos = 0; pos < totalPoliticalParties; pos++) {
          const name = response2[0][pos];
          let candidates = [];
          for (let cand = 0; cand < response2[1][pos].length; cand++) {
            candidates.push({
              value: response2[1][pos][cand],
              label: response2[1][pos][cand],
            });
          }
          statePoliticalParties.push({
            candidates: candidates,
            name: name,
            partieLogo: [],
          });
        }

        //Service for votingLists
        const response3 = await this.state.contract.methods
          .getVotingLists()
          .call();
        let stateVotingList = [];
        const totalVotingList = response3[0].length;
        for (let pos = 0; pos < totalVotingList; pos++) {
          const title = response3[0][pos];
          const description = response3[1][pos];
          let display = true;
          let label;
          if (response3[2][pos].localeCompare("unico") === 0) {
            label = "Único candidato";
          } else {
            label = "Múltiples candidatos";
          }
          const type = { value: response3[2][pos], label: label };

          let politicalParties = [];
          for (let posPP = 0; posPP < totalPoliticalParties; posPP++) {
            const candidates = statePoliticalParties[posPP].candidates;
            const namePP = statePoliticalParties[posPP].name;
            let selectedCandidates = [];
            let isChecked = true;
            const candidatesIndexVotingList = await this.state.contract.methods
              .getCandidatesToVotingList()
              .call();
            console.log(candidatesIndexVotingList);
            const candPoliticalPartie = candidatesIndexVotingList[posPP];

            //Falta crear selectedCandidates y isChecked
            for (let index = 0; index < candPoliticalPartie.length; index++) {
              // console.log("cand: ", candidatesIndexVotingList[posPP]," ", candidatesIndexVotingList[posPP][index], posPP, "valor: ",statePoliticalParties[posPP].candidates[index]);
              // const aux = posPP + 1;
              // console.log(parseInt(candidatesIndexVotingList[posPP][index]) === aux,  aux,parseInt(candidatesIndexVotingList[posPP][index]));
              if (parseInt(candPoliticalPartie[index]) === pos + 1) {
                console.log(
                  "entro: [",
                  pos,
                  "] => ",
                  statePoliticalParties[posPP].candidates[index]
                );
                selectedCandidates.push(
                  statePoliticalParties[posPP].candidates[index]
                );
              }
            }
            if (selectedCandidates.length === 0) {
              isChecked = false;
            }
            politicalParties.push({
              candidates: candidates,
              name: namePP,
              isChecked: isChecked,
              selectedCandidates: selectedCandidates,
            });
          }
          stateVotingList.push({
            display: display,
            title: title,
            description: description,
            type: type,
            politicalParties: politicalParties,
          });
        }
        console.log(response2, response3);
        console.log(
          stateGeneralInformation,
          statePoliticalParties,
          stateVotingList
        );
        //Seteamos el local State
        await this.props.handleSaveElectoralProcess(stateGeneralInformation);
        await this.props.handleSavePoliticalParties(statePoliticalParties);
        await this.props.handleSaveListVoting(stateVotingList);
      }
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
    }

    console.log(this.state);
    this.forceUpdate();
    this.setState({ loading: false });
  };
  render() {
    const elector = this.state.elector;
    console.log("elector", elector);
    console.log(this.state, this.props);
    if (this.state.loading) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div>
        <Navbar
          web3={this.state.web3}
          contract={this.state.contract}
          accounts={this.state.accounts}
          handleLogOut={this.handleLogOut}
          flagReadInstructions={this.state.flagReadInstructions}
          handleNextComponent={this.handleNextComponent}
          handleFlagReadInstructions={this.handleFlagReadInstructions}
        />

        <this.state.workingSpace
          handleNextComponent={this.handleNextComponent}
          flagReadInstructions={this.state.flagReadInstructions}
          handleFlagReadInstructions={this.handleFlagReadInstructions}
          web3={this.state.web3}
          contract={this.state.contract}
          accounts={this.state.accounts}
        />
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    politicalPartiesList: state.politicalPartiesList,
    electoralProcess: state.electoralProcess,
    votingList: state.votingList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSaveElectoralProcess: (value) =>
      dispatch(handleSaveElectoralProcess(value)),
    handleSavePoliticalParties: (value) =>
      dispatch(handleSavePoliticalParties(value)),
    handleSaveListVoting: (value) => dispatch(handleSaveListVoting(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeElector);
